package it.univr.concurrency.simpleelections.view;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import net.jcip.annotations.UiThread;

public interface View {
	public final ScheduledExecutorService exec
		= Executors.newScheduledThreadPool(2);

	// 3: change your display
	@UiThread
	void askForNewParty();

	@UiThread 
	void reportSaved();

	// 4: I've changed
	@UiThread
	void onModelChanged();
}