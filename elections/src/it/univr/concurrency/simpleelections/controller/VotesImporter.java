package it.univr.concurrency.simpleelections.controller;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicReference;

import it.univr.concurrency.simpleelections.model.Model;

class VotesImporter extends Thread {
	private final Model model;
	private final static String SERVER = "https://mysterious-escarpment-70352.herokuapp.com/";

	VotesImporter(Model model) {
		this.model = model;
	}

	@Override
	public void run() {
		AtomicReference<String> ar = new AtomicReference<>();

		try {
			// @WorkerThread -> @UiThread
			EventQueue.invokeAndWait(() -> {
				String parties = "";

				for (String party: model.getParties()) {
					if (!parties.isEmpty())
						parties += ',';

					parties += party;
				}
				
				ar.set(parties);
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}

		try {
			URL url = new URL(SERVER + "sendvotes?howmany=1000&parties=" + URLEncoder.encode(ar.get(), "UTF-8"));
			URLConnection conn = url.openConnection();

			try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
				String party;
				
				while ((party = in.readLine()) != null) {
					String party2 = party;
					// @WorkerThread -> @UiThread
					EventQueue.invokeLater(() -> model.addVotesTo(party2, 1));
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	};
}
