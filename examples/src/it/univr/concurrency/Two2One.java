package it.univr.concurrency;

public class Two2One extends Thread {
	private final static int NUM = 4;
	private static long nice;
	private static long ugly;

	@Override
	public void run() {
		long limit = 10000000L / NUM;
		for (long count = 0; count < limit; count++) {
			synchronized (Two2One.class) {
				nice++;
				ugly++;
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		Two2One[] racers = new Two2One[NUM];
		for (int pos = 0; pos < NUM; pos++)
			racers[pos] = new Two2One();

		long start = System.currentTimeMillis();
		for (Two2One racer: racers)
			racer.start();
		for (Two2One racer: racers)
			racer.join();

		System.out.println("time required: " + (System.currentTimeMillis() - start) + "ms");
		System.out.println("nice = " + nice + " ugly = " + ugly);
	}
}
