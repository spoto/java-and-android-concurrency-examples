package it.univr.concurrency;

import java.util.Vector;

import net.jcip.annotations.ThreadSafe;

/**
 * SafeVectorHelpers
 * <p/>
 * Compound actions on Vector using client-side locking
 *
 * @author Brian Goetz and Tim Peierls
 */
@ThreadSafe
public class SafeVectorHelpers {
    public static <T> T getLast(Vector<T> list) {
        synchronized (list) {
            int lastIndex = list.size() - 1;
            return list.get(lastIndex);
        }
    }

    public static <T> void deleteLast(Vector<T> list) {
        synchronized (list) {
            int lastIndex = list.size() - 1;
            list.remove(lastIndex);
        }
    }
}