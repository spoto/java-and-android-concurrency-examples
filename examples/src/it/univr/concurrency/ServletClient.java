package it.univr.concurrency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * ServletClient
 * <p/>
 * A very simple Java client of a servlet
 *
 * @author Fausto Spoto
 */
public class ServletClient {
	public final static String SERVER = "https://limitless-bayou-56277.herokuapp.com/StatelessFactorizer?number=250";

	public static void main(String[] args) throws MalformedURLException, IOException {
		URL url = new URL(SERVER);
		URLConnection conn = url.openConnection();

		try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
			String response;
			while ((response = in.readLine()) != null)
				System.out.println(response);
		}
	}
}