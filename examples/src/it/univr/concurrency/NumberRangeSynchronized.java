package it.univr.concurrency;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * NumberRangeSynchronized
 * <p/>
 * Number range class that protects its invariants through synchronization
 *
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */

@ThreadSafe
public class NumberRangeSynchronized {
    // INVARIANT: lower <= upper
    private @GuardedBy("this") int lower = 0;
    private @GuardedBy("this") int upper = 0;

    public synchronized void setLower(int i) {
        if (i > upper)
            throw new IllegalArgumentException("can't set lower to " + i + " > upper");
        lower = i;
    }

    public synchronized void setUpper(int i) {
        if (i < lower)
            throw new IllegalArgumentException("can't set upper to " + i + " < lower");
        upper = i;
    }

    public synchronized boolean isInRange(int i) {
        return i >= lower && i <= upper;
    }
}