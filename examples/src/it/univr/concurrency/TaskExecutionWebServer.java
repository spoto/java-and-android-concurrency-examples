package it.univr.concurrency;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import net.jcip.annotations.ThreadSafe;

/**
 * TaskExecutionWebServer
 * <p/>
 * Web server using a thread pool
 *
 * @author Brian Goetz and Tim Peierls
 */

@ThreadSafe
public class TaskExecutionWebServer {
    private static final int NTHREADS = 100;
    private static final Executor exec = Executors.newFixedThreadPool(NTHREADS);

    public static void main(String[] args) throws IOException {
    	try (ServerSocket socket = new ServerSocket(80)) {
    		while (true) {
    			Socket connection = socket.accept();
    			exec.execute(() -> handleRequest(connection));
    		}
    	}
    }

    private static void handleRequest(Socket connection) {
        // request-handling logic here
    }
}