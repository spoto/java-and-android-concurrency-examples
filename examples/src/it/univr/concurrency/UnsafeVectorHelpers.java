package it.univr.concurrency;

import java.util.Vector;

import net.jcip.annotations.NotThreadSafe;

/**
 * UnsafeVectorHelpers
 * <p/>
 * Compound actions on a Vector that may produce confusing results
 *
 * @author Brian Goetz and Tim Peierls
 */
@NotThreadSafe
public class UnsafeVectorHelpers {
    public static <T> T getLast(Vector<T> list) {
        int lastIndex = list.size() - 1;
        return list.get(lastIndex);
    }

    public static <T> void deleteLast(Vector<T> list) {
        int lastIndex = list.size() - 1;
        list.remove(lastIndex);
    }
}