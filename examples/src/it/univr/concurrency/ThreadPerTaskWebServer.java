package it.univr.concurrency;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import net.jcip.annotations.ThreadSafe;

/**
 * ThreadPerTaskWebServer
 * <p/>
 * Web server that starts a new thread for each request
 *
 * @author Brian Goetz and Tim Peierls
 */

@ThreadSafe
public class ThreadPerTaskWebServer {
	public static void main(String[] args) throws IOException {
		try (ServerSocket socket = new ServerSocket(80)) {
			while (true) {
				Socket connection = socket.accept();
				Runnable task = () -> handleRequest(connection);
				new Thread(task).start();
			}
		}
	}

    private static void handleRequest(Socket connection) {
        // request-handling logic here
    }
}