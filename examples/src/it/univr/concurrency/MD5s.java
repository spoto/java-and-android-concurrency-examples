package it.univr.concurrency;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import net.jcip.annotations.ThreadSafe;

/**
 * MD5s
 * <p/>
 * Producer and consumer tasks for generating MD5 digests of files in directories
 *
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */

@ThreadSafe
public class MD5s {
	private final BlockingQueue<File> queue = new LinkedBlockingQueue<>(BOUND);
	private final Set<Thread> indexers = new HashSet<>();

	private MD5s() {
		for (int i = 0; i < N_CONSUMERS; i++)
			indexers.add(new Thread(new Indexer()));
	}

	public static MD5s mk() {
		MD5s pc = new MD5s();
		for (Thread indexer: pc.indexers)
			indexer.start();

		return pc;
	}

	public static void main(String[] args) throws InterruptedException {
		File[] roots = new File[args.length];

		int pos = 0;
		for (String arg: args)
			roots[pos++] = new File(arg);
		
		MD5s pc = mk();
		CountDownLatch counter = new CountDownLatch(roots.length);
		pc.startIndexing(roots, counter);
		counter.await();
		pc.stop();
	}

	public void startIndexing(File[] roots, CountDownLatch counter) {
	    FileFilter filter = file -> true;
	    for (File root: roots)
	        new Thread(new FileCrawler(filter, root, counter)).start();
	}
	
	public void stop() {
		//TODO
	}

	public String getMD5Of(String fileName) {
		//TODO
		return null;
	}

	private boolean alreadyIndexed(File file) {
		//TODO
		return false;
    }

    @ThreadSafe
    private class FileCrawler implements Runnable {
        private final FileFilter fileFilter;
        private final File root;
        private final CountDownLatch counter;

        public FileCrawler(FileFilter fileFilter, File root, CountDownLatch counter) {
            this.root = root;
            this.fileFilter = file -> (file.isDirectory() || fileFilter.accept(file));
            this.counter = counter;
        }

        @Override
        public void run() {
            try {
                crawl(root);
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            finally {
            	counter.countDown();
            }
        }

        private void crawl(File root) throws InterruptedException {
            File[] entries = root.listFiles(fileFilter);
            if (entries != null)
                for (File entry: entries)
                    if (entry.isDirectory())
                        crawl(entry);
                    else if (!alreadyIndexed(entry))
                    	queue.put(entry);
        }
    }

    @ThreadSafe
    private class Indexer implements Runnable {

		@Override
		public void run() {
		    try {
		        while (true)
		            index(queue.take());
		    }
		    catch (InterruptedException e) {
		    	//TODO
		    	Thread.currentThread().interrupt();
		    }
		}

		private void index(File file) {
			//TODO
		}

		private String md5(File file) throws IOException {
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				try (InputStream is = new FileInputStream(file);
				     DigestInputStream dis = new DigestInputStream(is, md)) {
		
					byte[] buffer = new byte[1024];
					while (dis.read(buffer) > 0);
				}
		
				byte[] digest = md.digest();
				return javax.xml.bind.DatatypeConverter.printHexBinary(digest);
			}
			catch (NoSuchAlgorithmException e) {
				// impossible, MD5 exists
				return "no MD5 algorithm found";
			}
		}
    }

    private static final int BOUND = 10;
    private static final int N_CONSUMERS = Runtime.getRuntime().availableProcessors();
}