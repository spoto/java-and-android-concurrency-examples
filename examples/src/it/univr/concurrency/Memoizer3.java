package it.univr.concurrency;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import net.jcip.annotations.ThreadSafe;

/**
 * Memoizer3
 * <p/>
 * Using FutureTask in ConcurrentHashMap
 *
 * @author Brian Goetz and Tim Peierls
 */

@ThreadSafe
public class Memoizer3<A, V> implements Computable<A, V> {
    private final Map<A, Future<V>> cache = new ConcurrentHashMap<>();
    private final Computable<A, V> c;

    public Memoizer3(Computable<A, V> c) {
        this.c = c;
    }

    public V compute(A arg) throws InterruptedException {
    	Future<V> f = cache.get(arg);
    	if (f == null) {
    		FutureTask<V> ft = new FutureTask<>(() -> c.compute(arg));
    		cache.put(arg, f = ft);
    		ft.run(); // call to c.compute happens here
    	}
    	try {
    		return f.get();
    	}
    	catch (ExecutionException e) {
    		throw LaunderThrowable.launderThrowable(e.getCause());
    	}
    }
}