package it.univr.concurrency;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.jcip.annotations.ThreadSafe;

/**
 * ProducerConsumer
 * <p/>
 * Producer and consumer tasks in a desktop search application
 *
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */
@ThreadSafe
public class ProducerConsumer {
	private final BlockingQueue<File> queue = new LinkedBlockingQueue<>(BOUND);

    public void startIndexing(File[] roots) {
	    FileFilter filter = file -> true;
	
	    for (File root: roots)
	        new Thread(new FileCrawler(filter, root)).start();
	
	    for (int i = 0; i < N_CONSUMERS; i++)
	        new Thread(new Indexer()).start();
	}

	private boolean alreadyIndexed(File f) {
        return false;
    }

    private void index(File file) {
	    // Index the file...
	}

	@ThreadSafe
    private class FileCrawler implements Runnable {
        private final FileFilter fileFilter;
        private final File root;

        public FileCrawler(FileFilter fileFilter, File root) {
            this.root = root;
            this.fileFilter = file -> (file.isDirectory() || fileFilter.accept(file));
        }

        @Override
        public void run() {
            try {
                crawl(root);
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        private void crawl(File root) throws InterruptedException {
            File[] entries = root.listFiles(fileFilter);
            if (entries != null)
                for (File entry: entries)
                    if (entry.isDirectory())
                        crawl(entry);
                    else if (!alreadyIndexed(entry))
                    	queue.put(entry);
        }
    }

    @ThreadSafe
    private class Indexer implements Runnable {

    	@Override
    	public void run() {
            try {
                while (true)
                    index(queue.take());
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private static final int BOUND = 10;
    private static final int N_CONSUMERS = Runtime.getRuntime().availableProcessors();
}