package it.univr.concurrency;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import net.jcip.annotations.ThreadSafe;

/**
 * CellularAutomata
 *
 * Coordinating computation in a cellular automaton with CyclicBarrier
 *
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */

@ThreadSafe
public abstract class CellularAutomata {
    private final Board mainBoard;
    private final CyclicBarrier barrier;
    private final Worker[] workers;

    public CellularAutomata(Board board) {
    	int count = Runtime.getRuntime().availableProcessors();

    	this.mainBoard = board;
        this.barrier = new CyclicBarrier(count, () -> mainBoard.commitNewValues());
        this.workers = new Worker[count];
        for (int i = 0; i < count; i++)
            workers[i] = new Worker(mainBoard.getSubBoard(count, i));
    }

    protected abstract int computeValue(Board board, int x, int y);

	private class Worker implements Runnable {
        private final Board board;

        public Worker(Board board) { this.board = board; }

        public void run() {
            while (!board.hasConverged()) {
                for (int x = 0; x < board.getMaxX(); x++)
                    for (int y = 0; y < board.getMaxY(); y++)
                        board.setNewValue(x, y, computeValue(board, x, y));
                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException ex) {
                    return;
                }
            }
        }
    }

    public void start() throws InterruptedException {
        for (Worker worker: workers)
            new Thread(worker).start();

        mainBoard.waitForConvergence();
    }

    public interface Board {
        int getMaxX();
        int getMaxY();
        int getValue(int x, int y);
        void setNewValue(int x, int y, int value);
        void commitNewValues();
        boolean hasConverged();
        void waitForConvergence() throws InterruptedException; // blocking
        Board getSubBoard(int numPartitions, int index);
    }
}