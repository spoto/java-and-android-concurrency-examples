package it.univr.matrix;

import java.util.Random;

import net.jcip.annotations.Immutable;

@Immutable
public class Matrix {
	private final double[][] elements;
	private final static Random random = new Random();

	public Matrix(int m, int n) {
		if (m <= 0 || n <= 0)
			throw new IllegalArgumentException("dimensions should be positive");

		this.elements = new double[m][n];
		for (int x = 0; x < n; x++)
			for (int y = 0; y < m; y++)
				elements[y][x] = random.nextDouble() * 100.0 - 50.0;
	}

	private Matrix(Matrix left, Matrix right) {
		int m = left.getM();
		int p = left.getN();
		if (p != right.getM())
			throw new IllegalArgumentException("dimensions do not march");

		int n = right.getN();

		this.elements = new double[m][n];
		for (int x = 0; x < n; x++)
			for (int y = 0; y < m; y++) {
				double sum = 0.0;
				for (int k = 0; k < p; k++)
					sum += left.elements[y][k] * right.elements[k][x];

				this.elements[y][x] = sum;
			}
				
	}

	public int getM() {
		return elements.length;
	}

	public int getN() {
		return elements[0].length;
	}

	public Matrix times(Matrix other) {
		return new Matrix(this, other);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int n = getN(), m = getM(), y = 0; y < m; y++) {
			for (int x = 0; x < n; x++)
				sb.append(String.format("%10.2f", elements[y][x]));

			sb.append('\n');
		}
		
		return sb.toString();
	}

	private static void randomMultiplication() {
		// random dimensions between 20 and 200, inclusive
		int m = random.nextInt(281) + 20;
		int p = random.nextInt(281) + 20;
		int n = random.nextInt(281) + 20;
		
		Matrix m1 = new Matrix(m, p);
		Matrix m2 = new Matrix(p, n);
		m1.times(m2);
	}

	public static void main(String[] args) {
		Matrix m1 = new Matrix(4, 5);
		Matrix m2 = new Matrix(5, 8);
		Matrix m3 = m1.times(m2);
		System.out.println("m1:\n" + m1);
		System.out.println("m2:\n" + m2);
		System.out.println("m1 x m2:\n" + m3);

		System.out.println("now performing " + N + " random multiplications");
		long startTime = System.currentTimeMillis();

		for (int counter = 0; counter < N; counter++)
			randomMultiplication();

		long endTime = System.currentTimeMillis();

		System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
	}

	public final static int N = 10000;
}