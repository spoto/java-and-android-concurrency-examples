package it.univr.matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import it.univr.concurrency.LaunderThrowable;
import net.jcip.annotations.Immutable;

@Immutable
public class ExecutorMatrix {
	private final double[][] elements;
	private final static int nCores = Runtime.getRuntime().availableProcessors();
	private final static ExecutorService exec = Executors.newFixedThreadPool(nCores);

	public static void shutdown() {
		exec.shutdown();
	}

	public ExecutorMatrix(int m, int n) throws InterruptedException {
		if (m <= 0 || n <= 0)
			throw new IllegalArgumentException("dimensions should be positive");

		this.elements = new double[m][n];
		
		class Init implements Callable<Void> {
			private final int k;
			
			private Init(int k) {
				this.k = k;
			}

			@Override
			public Void call() throws Exception {
				Random random = new Random();

				for (int x = 0; x < n; x++)
					if (x % nCores == k)
						for (int y = 0; y < m; y++)
							elements[y][x] = random.nextDouble()
								* 100.0 - 50.0;

				return null;
			}
		};

		List<Callable<Void>> tasks = new ArrayList<>();
		for (int pos = 0; pos < nCores; pos++)
			tasks.add(new Init(pos));
		
		List<Future<Void>> futures = exec.invokeAll(tasks);
		for (Future<Void> future: futures)
			try {
				future.get();
			}
			catch (ExecutionException e) {
				throw LaunderThrowable.launderThrowable(e.getCause());
			}
	}

	private ExecutorMatrix(ExecutorMatrix left, ExecutorMatrix right)
			throws InterruptedException {
		int m = left.getM();
		int p = left.getN();
		if (p != right.getM())
			throw new IllegalArgumentException("dimensions do not march");

		int n = right.getN();

		this.elements = new double[m][n];

		class Multiplier implements Callable<Void> {
			private final int k;

			private Multiplier(int k) {
				this.k = k;
			}

			@Override
			public Void call() {
				for (int x = 0; x < n; x++)
					if (x % nCores == k)
						for (int y = 0; y < m; y++) {
							double sum = 0.0;
							for (int k = 0; k < p; k++)
								sum += left.elements[y][k] * right.elements[k][x];

							elements[y][x] = sum;
						}	

				return null;
			}
		}

		List<Callable<Void>> tasks = new ArrayList<>();
		for (int pos = 0; pos < nCores; pos++)
			tasks.add(new Multiplier(pos));
		
		List<Future<Void>> futures = exec.invokeAll(tasks);
		for (Future<Void> future: futures)
			try {
				future.get();
			}
			catch (ExecutionException e) {
				throw LaunderThrowable.launderThrowable(e.getCause());
			}
	}

	public int getM() {
		return elements.length;
	}

	public int getN() {
		return elements[0].length;
	}

	public ExecutorMatrix times(ExecutorMatrix other)
		throws InterruptedException {
		return new ExecutorMatrix(this, other);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int n = getN(), m = getM(), y = 0; y < m; y++) {
			for (int x = 0; x < n; x++)
				sb.append(String.format("%10.2f", elements[y][x]));

			sb.append('\n');
		}
		
		return sb.toString();
	}

	private static void randomMultiplication()
		throws InterruptedException {
		Random random = new Random();
		// random dimensions between 20 and 300, inclusive
		int m = random.nextInt(281) + 20;
		int p = random.nextInt(281) + 20;
		int n = random.nextInt(281) + 20;
		
		ExecutorMatrix m1 = new ExecutorMatrix(m, p);
		ExecutorMatrix m2 = new ExecutorMatrix(p, n);
		m1.times(m2);
	}

	public static void main(String[] args) throws InterruptedException {
		ExecutorMatrix m1 = new ExecutorMatrix(4, 5);
		ExecutorMatrix m2 = new ExecutorMatrix(5, 8);
		ExecutorMatrix m3 = m1.times(m2);
		System.out.println("m1:\n" + m1);
		System.out.println("m2:\n" + m2);
		System.out.println("m1 x m2:\n" + m3);

		System.out.println("now performing " + N + " random multiplications");
		long startTime = System.currentTimeMillis();

		for (int counter = 0; counter < N; counter++)
			randomMultiplication();

		long endTime = System.currentTimeMillis();

		System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
		shutdown();
	}

	public final static int N = 10000;
}