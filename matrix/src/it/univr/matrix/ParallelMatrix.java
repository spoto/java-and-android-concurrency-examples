package it.univr.matrix;

import java.util.Random;

import net.jcip.annotations.Immutable;

@Immutable
public class ParallelMatrix {
	private final double[][] elements;
	private final static Random random = new Random();

	public ParallelMatrix(int m, int n) {
		if (m <= 0 || n <= 0)
			throw new IllegalArgumentException("dimensions should be positive");

		this.elements = new double[m][n];
		for (int x = 0; x < n; x++)
			for (int y = 0; y < m; y++)
				elements[y][x] = random.nextDouble() * 100.0 - 50.0;
	}

	private ParallelMatrix(ParallelMatrix left, ParallelMatrix right)
			throws InterruptedException {
		int m = left.getM();
		int p = left.getN();
		if (p != right.getM())
			throw new IllegalArgumentException("dimensions do not march");

		int n = right.getN();

		this.elements = new double[m][n];

		int nThreads = Runtime.getRuntime().availableProcessors();

		class Multiplier extends Thread {
			private final int k;

			private Multiplier(int k) {
				this.k = k;
			}

			@Override
			public void run() {
				for (int x = 0; x < n; x++)
					if (x % nThreads == k)
						for (int y = 0; y < m; y++) {
							double sum = 0.0;
							for (int k = 0; k < p; k++)
								sum += left.elements[y][k] * right.elements[k][x];

							elements[y][x] = sum;
						}	
			}
		}

		Multiplier[] multipliers = new Multiplier[nThreads];
		for (int pos = 0; pos < nThreads; pos++)
			(multipliers[pos] = new Multiplier(pos)).start();
		
		for (Multiplier worker: multipliers)
			worker.join();
	}

	public int getM() {
		return elements.length;
	}

	public int getN() {
		return elements[0].length;
	}

	public ParallelMatrix times(ParallelMatrix other)
		throws InterruptedException {
		return new ParallelMatrix(this, other);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int n = getN(), m = getM(), y = 0; y < m; y++) {
			for (int x = 0; x < n; x++)
				sb.append(String.format("%10.2f", elements[y][x]));

			sb.append('\n');
		}
		
		return sb.toString();
	}

	private static void randomMultiplication()
		throws InterruptedException {
		// random dimensions between 20 and 300, inclusive
		int m = random.nextInt(281) + 20;
		int p = random.nextInt(281) + 20;
		int n = random.nextInt(281) + 20;
		
		ParallelMatrix m1 = new ParallelMatrix(m, p);
		ParallelMatrix m2 = new ParallelMatrix(p, n);
		m1.times(m2);
	}

	public static void main(String[] args) throws InterruptedException {
		ParallelMatrix m1 = new ParallelMatrix(4, 5);
		ParallelMatrix m2 = new ParallelMatrix(5, 8);
		ParallelMatrix m3 = m1.times(m2);
		System.out.println("m1:\n" + m1);
		System.out.println("m2:\n" + m2);
		System.out.println("m1 x m2:\n" + m3);

		System.out.println("now performing " + N + " random multiplications");
		long startTime = System.currentTimeMillis();

		for (int counter = 0; counter < N; counter++)
			randomMultiplication();

		long endTime = System.currentTimeMillis();

		System.out.println("Elapsed time: " + (endTime - startTime) + "ms");
	}

	public final static int N = 10000;
}