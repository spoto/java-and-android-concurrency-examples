package it.univr.gameoflife;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class BoardImpl extends AbstractBoard {
	private final int[][] current;
	private final int[][] next;
	private final CountDownLatch converged = new CountDownLatch(1);

	public BoardImpl(int n, int m) {
		if (n < 1 || m < 1)
			throw new IllegalArgumentException();
		
		this.current = new int[n][m];
		this.next = new int[n][m];

		Random random = new Random();
		for (int x = 0; x < m; x++)
			for (int y = 0; y < n; y++)
				current[y][x] = random.nextInt(2);
	}

	@Override
	public int getMaxX() {
		return current[0].length;
	}

	@Override
	public int getMaxY() {
		return current.length;
	}

	@Override
	public int getValue(int x, int y) {
		if (x < 0 || y < 0 || x >= getMaxX() || y >= getMaxY())
			return 0;
		else
			return current[y][x];
	}

	@Override
	public void setNewValue(int x, int y, int value) {
		next[y][x] = value;
	}

	@Override
	public void commitNewValues() {
		System.out.println("-----------------");
		System.out.print(this);

		boolean changed = false;
		for (int x = 0; x < getMaxX(); x++)
			for (int y = 0; y < getMaxY(); y++)
				if (current[y][x] != next[y][x]) {
					current[y][x] = next[y][x];
					changed = true;
				}

		if (!changed)
			converged.countDown();
	}

	@Override
	public boolean hasConverged() {
		return converged.getCount() < 1;
	}

	@Override
	public void waitForConvergence() throws InterruptedException {
		converged.await();
	}

	@Override
	public String toString() {
		String result = "";

		for (int y = 0; y < getMaxY(); y++) {
			result += "|";
			for (int x = 0; x < getMaxX(); x++)
				result += getValue(x, y) == 0 ? " " : "*";

			result += "|\n";
		}

		return result;
	}
}