package it.univr.gameoflife;

import it.univr.concurrency.CellularAutomata;

public class GameOfLife extends CellularAutomata {

	public GameOfLife(int n, int m) {
		super(new BoardImpl(n, m));
	}

	@Override
	protected int computeValue(Board board, int x, int y) {
		int neighbors = 0;
		boolean alive = board.getValue(x, y) != 0;

		for (int dx = -1; dx <= 1; dx++)
			for (int dy = -1; dy <= 1; dy++)
				if (dx != 0 || dy != 0)
					if (board.getValue(x + dx, y + dy) != 0)
						neighbors++;

		if (alive)
			if (neighbors < 2 || neighbors > 3)
				return 0;
			else
				return 1;
		else
			if (neighbors == 3)
				return 1;
			else
				return 0;
	}

	public static void main(String[] args) throws InterruptedException {
		new GameOfLife(10, 10).start();
	}
}