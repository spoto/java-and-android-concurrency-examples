package it.univr.chat;

import java.util.ArrayList;
import java.util.List;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class Chat {

	@Immutable
	public static class Message {
		public final String author;
		public final String text;
		
		private Message(String author, String text) {
			this.author = author;
			this.text = text;
		}
	}

	private final List<Message> messages = new ArrayList<>();

	public void addMessage(String author, String text) {
		Message message = new Message(author, text);

		synchronized (messages) {
			messages.add(message);
		}
	}

	public interface Task {
		void process(Message message);
	}

	public void forTheLastMessages(int howMany, Task task) {
		List<Message> lastMessages;

		synchronized (messages) {
			int size = messages.size();
			// make a snapshot of the latest messages, then release the lock
			lastMessages = new ArrayList<>(messages.subList(Math.max(size - howMany, 0), size));
		}

		for (Message message: lastMessages)
			task.process(message);
	}
}