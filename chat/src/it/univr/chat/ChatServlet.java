package it.univr.chat;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class ChatServlet extends HttpServlet {

	private final static Object lock = new Object();

	protected final Chat getChat() {
		ServletContext context = getServletContext();

		Chat chat;
		
		synchronized (lock) {
			chat = (Chat) context.getAttribute("chat");
			if (chat == null)
				context.setAttribute("chat", chat = new Chat());
		}
		
		return chat;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}