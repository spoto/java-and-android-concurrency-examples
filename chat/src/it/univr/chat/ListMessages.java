package it.univr.chat;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ListMessages")
public class ListMessages extends ChatServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletOutputStream out = response.getOutputStream();
		String par = request.getParameter("howmany");
		if (par != null)
			try {
				int howMany = Integer.parseInt(par);
				getChat().forTheLastMessages(howMany, message -> dumpAsXML(out, message));
			}
			catch (NumberFormatException e) {}
	}

	private void dumpAsXML(ServletOutputStream out, Chat.Message message) {
		StringBuilder xml = new StringBuilder();
		xml.append("<message>\n");
		xml.append("  <author>\n");
		xml.append("    " + message.author + "\n");
		xml.append("  </author>\n");
		xml.append("  <text>\n");
		xml.append("    " + message.text + "\n");
		xml.append("  </text>\n");
		xml.append("</message>\n");

		try {
			out.print(xml.toString());
		}
		catch (IOException e) {}
	}
}