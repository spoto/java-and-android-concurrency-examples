package it.univr.factorizer.client;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FactorizerWindow extends JFrame {
	private final JTextArea textArea;

	public FactorizerWindow() {
		JPanel north = new JPanel();
		JTextField numberField = new JTextField();
		JButton go = new JButton("GO!");
		north.setLayout(new GridLayout(1, 2));
		north.add(numberField);
		north.add(go);
		add(north, BorderLayout.NORTH);

		textArea = new JTextArea(500, 400);
		JScrollPane scroll = new JScrollPane(textArea);
		add(scroll, BorderLayout.CENTER);

		go.addActionListener(e -> 
			{
				String text = numberField.getText();
				try {
					Integer number = Integer.parseInt(text);
					queryServlet(number);
				}
				catch (NumberFormatException ex) {
					textArea.append("number required\n");
				}
			});

		pack();
	}

	private void queryServlet(int number) {
		new ConnectionThread(number).start();
	}

	// change this to the actual server that you want to query
	private final static String SERVER
		= "https://chat-application-blasco991.herokuapp.com/StatelessFactorizer?number=";

	private class ConnectionThread extends Thread {
		private final int number;

		private ConnectionThread(int number) {
			this.number = number;
		}

		@Override
		public void run() {
			try {
				URL url = new URL(SERVER + number);
				URLConnection conn = url.openConnection();
				String result = "";

				try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
					String response;
					while ((response = in.readLine()) != null)
						result += response;
				}

				String print = result + "\n";
				EventQueue.invokeLater(() -> textArea.append(print));
			}
			catch (IOException e) {
				EventQueue.invokeLater(() -> textArea.append("exception: " + e + "\n"));
			}
		}
	}
}
