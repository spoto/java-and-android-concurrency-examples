package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.ThreadSafe;

/**
 * StatelessFactorizer
 *
 * A stateless servlet
 * 
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */

@ThreadSafe
@WebServlet("/StatelessFactorizer")
public class StatelessFactorizer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		BigInteger[] factors = factor(number);
		encodeIntoResponse(response, factors);
	}

	protected BigInteger extractFromRequest(HttpServletRequest request) {
		return new BigInteger(request.getParameter("number"));
	}

	private final static BigInteger TWO = new BigInteger(new byte[] { 2 });

	protected BigInteger[] factor(BigInteger number) {
		List<BigInteger> factors = new ArrayList<>();
	
		BigInteger i = TWO;

		while (i.multiply(i).compareTo(number) <= 0) {
			while (number.remainder(i).equals(BigInteger.ZERO)) {
				factors.add(i);
				number = number.divide(i);
			}

			i = i.add(BigInteger.ONE);
		}

		if (number.compareTo(TWO) >= 0)
			factors.add(number);

		return factors.toArray(new BigInteger[factors.size()]);
	}

	protected void encodeIntoResponse(HttpServletResponse response, BigInteger[] factors) throws IOException {
		ServletOutputStream out = response.getOutputStream();
		out.println(Arrays.toString(factors));
	}

	protected void encodeErrorIntoResponse(HttpServletResponse response, String message) throws IOException {
		ServletOutputStream out = response.getOutputStream();
		out.println(message);
	}
}