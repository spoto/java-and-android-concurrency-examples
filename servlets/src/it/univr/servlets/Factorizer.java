package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.univr.concurrency.Computable;
import it.univr.concurrency.Memoizer;
import net.jcip.annotations.ThreadSafe;

/**
 * Factorizer
 * <p/>
 * Factorizing servlet that caches results using Memoizer
 *
 * @author Brian Goetz and Tim Peierls
 */
@ThreadSafe
@WebServlet("/Factorizer")
public class Factorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private final Computable<BigInteger, BigInteger[]> cache = new Memoizer<>(arg -> factor(arg));

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);

		try {
			encodeIntoResponse(response, cache.compute(number));
		}
		catch (InterruptedException e) {
			encodeErrorIntoResponse(response, "factorization interrupted");
		}
	}
}