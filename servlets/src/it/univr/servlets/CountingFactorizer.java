package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.ThreadSafe;

/**
 * CountingFactorizer
 *
 * Servlet that counts requests using AtomicLong
 *
 * @author Brian Goetz and Tim Peierls and Fausto Spoto
 */

@ThreadSafe
@WebServlet("/CountingFactorizer")
public class CountingFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;
	private final AtomicLong count = new AtomicLong(0L);

	public long getCount() {
		return count.get();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		BigInteger[] factors = factor(number);
		count.incrementAndGet();
		encodeIntoResponse(response, factors);
	}
}