package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.NotThreadSafe;

/**
 * UnsafeCachingFactorizer
 *
 * Servlet that attempts to cache its last result without adequate atomicity
 *
 * @author Brian Goetz and Tim Peierls and Fausto Spoto
 */

@NotThreadSafe
@WebServlet("/UnsafeCachingFactorizer")
public class UnsafeCachingFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private final AtomicReference<BigInteger> lastNumber = new AtomicReference<>();
	private final AtomicReference<BigInteger[]> lastFactors = new AtomicReference<>();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		if (number.equals(lastNumber.get()))
            encodeIntoResponse(response, lastFactors.get());
        else {
            BigInteger[] factors = factor(number);
            lastNumber.set(number);
            lastFactors.set(factors);
            encodeIntoResponse(response, factors);
        }
	}
}