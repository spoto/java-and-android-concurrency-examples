package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * SynchronizedFactorizer
 *
 * Servlet that caches last result, but with unnacceptably poor concurrency
 *
 * @author Brian Goetz and Tim Peierls and Fausto Spoto
 */

@ThreadSafe
@WebServlet("/SynchronizedFactorizer")
public class SynchronizedFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private @GuardedBy("this") BigInteger lastNumber;
	private @GuardedBy("this") BigInteger[] lastFactors;

	@Override
	protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		if (number.equals(lastNumber))
            encodeIntoResponse(response, lastFactors);
        else {
            BigInteger[] factors = factor(number);
            lastNumber = number;
            lastFactors = factors;
            encodeIntoResponse(response, factors);
        }
	}
}