package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.ThreadSafe;

/**
 * VolatileCachedFactorizer
 * <p/>
 * Caching the last result using a volatile reference to an immutable holder object
 *
 * @author Brian Goetz, Tim Peierls and Fausto Spoto
 */
@ThreadSafe
public class VolatileCachedFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private volatile OneValueCache cache = new OneValueCache(null, null);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BigInteger i = extractFromRequest(request);
        BigInteger[] factors = cache.getFactors(i);
        if (factors == null) {
            factors = factor(i);
            cache = new OneValueCache(i, factors);
        }
        encodeIntoResponse(response, factors);
    }
}
