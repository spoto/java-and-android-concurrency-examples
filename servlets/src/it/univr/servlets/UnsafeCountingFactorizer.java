package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.NotThreadSafe;

/**
 * UnsafeCountingFactorizer
 *
 * Servlet that counts requests without the necessary synchronization
 *
 * @author Brian Goetz and Tim Peierls and Fausto Spoto
 */

@NotThreadSafe
@WebServlet("/UnsafeCountingFactorizer")
public class UnsafeCountingFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private long count = 0;

    public long getCount() {
        return count;
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		BigInteger[] factors = factor(number);
		++count;
		encodeIntoResponse(response, factors);
	}
}