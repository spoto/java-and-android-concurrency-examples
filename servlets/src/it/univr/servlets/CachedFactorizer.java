package it.univr.servlets;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * CachedFactorizer
 *
 * Servlet that caches its last request and result
 *
 * @author Brian Goetz and Tim Peierls and Fausto Spoto
 */

@ThreadSafe
@WebServlet("/CachedFactorizer")
public class CachedFactorizer extends StatelessFactorizer {
	private static final long serialVersionUID = 1L;

	private @GuardedBy("this") BigInteger lastNumber;
	private @GuardedBy("this") BigInteger[] lastFactors;
	private @GuardedBy("this") long hits;
	private @GuardedBy("this") long cacheHits;

	public synchronized long getHits() {
		return hits;
	}

	public synchronized double getCacheHitsRatio() {
		return cacheHits / (double) hits;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BigInteger number = extractFromRequest(request);
		BigInteger[] factors = null;

		synchronized (this) {
			++hits;
			if (number.equals(lastNumber)) {
				++cacheHits;
				factors = lastFactors.clone();
			}
		}

		if (factors == null) {
			factors = factor(number);

			synchronized (this) {
				lastNumber = number;
				lastFactors = factors.clone();
			}
        }

		encodeIntoResponse(response, factors);
	}
}